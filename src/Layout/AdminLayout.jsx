import React from "react";
import { localServ } from "../services/localStoreService";
import AdminHeader from "../Components/AdminHeader/AdminHeader";
import AdminSideBar from "../Components/AdminSideBar/AdminSideBar";

export default function AdminLayout({ contentPage }) {
  if (!localServ.getUser() || localServ.getUser().maLoaiNguoiDung !== "GV") {
    window.location.href = "/login";
  }
  return (
    <div className="min-h-screen bg-white dark:bg-gray-900">
      <AdminSideBar />
      <AdminHeader />
      <div className="p-4 lg:ml-64">{contentPage}</div>
    </div>
  );
}
