import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isSideBarOpen: false,
};

const navigateSlice = createSlice({
  name: "navigateSlice",
  initialState,
  reducers: {
    setIsSideBarOpen: (state, action) => {
      state.isSideBarOpen = action.payload;
    },
  },
});

export const { setIsSideBarOpen } = navigateSlice.actions;

export default navigateSlice.reducer;
