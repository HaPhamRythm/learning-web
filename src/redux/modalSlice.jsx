import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isAddUser: false,
  isRegisterUser: false,
  userId: "",
  isEditUser: false,

  userAccessTokenEdit: "",
  passwordEdit: "",
  userIdEdit: "",
  isUpdateUser: false,
  userAccessToken: "",

  isRegisterCourse: false,
  courseId: "",
  isAddCourse: false,
  isEditCourse: false,
  courseIdEdit: "",

  isDeleteCourse: false,
  courseIdDelete: "",
};

const modalSlice = createSlice({
  name: "modalSlice",
  initialState,
  reducers: {
    setIsAddUser: (state, action) => {
      state.isAddUser = action.payload;
    },

    setIsRegisterUser: (state, action) => {
      state.isRegisterUser = action.payload;
    },

    setUserId: (state, action) => {
      state.userId = action.payload;
    },

    setIsEditUser: (state, action) => {
      state.isEditUser = action.payload;
    },

    setUserAccessTokenEdit: (state, action) => {
      state.userAccessTokenEdit = action.payload;
    },

    setPasswordEdit: (state, action) => {
      state.passwordEdit = action.payload;
    },
    setUserIdEdit: (state, action) => {
      state.userIdEdit = action.payload;
    },
    setIsUpdateUser: (state, action) => {
      state.isUpdateUser = action.payload;
    },
    setUserAccessToken: (state, action) => {
      state.userAccessToken = action.payload;
    },

    setIsRegisterCourse: (state, action) => {
      state.isRegisterCourse = action.payload;
    },

    setCourseId: (state, action) => {
      state.courseId = action.payload;
    },

    setIsAddCourse: (state, action) => {
      state.isAddCourse = action.payload;
    },

    setIsEditCourse: (state, action) => {
      state.isEditCourse = action.payload;
    },

    setCourseIdEdit: (state, action) => {
      state.courseIdEdit = action.payload;
    },

    setIsDeleteCourse: (state, action) => {
      state.isDeleteCourse = action.payload;
    },

    setCourseIdDelete: (state, action) => {
      state.courseIdDelete = action.payload;
    },
  },
});

export const {
  setIsAddUser,
  setIsRegisterUser,
  setUserId,
  setIsEditUser,
  setUserAccessTokenEdit,
  setPasswordEdit,
  setUserIdEdit,
  setIsUpdateUser,
  setUserAccessToken,

  setIsRegisterCourse,
  setCourseId,
  setIsAddCourse,
  setIsEditCourse,
  setCourseIdEdit,

  setIsDeleteCourse,
  setCourseIdDelete,
} = modalSlice.actions;

export default modalSlice.reducer;
