import { useMediaQuery } from "react-responsive";

export const Desktop = ({ children }) => {
  const isDeskTop = useMediaQuery({ minWidth: 1024 });
  return isDeskTop ? children : null;
};
