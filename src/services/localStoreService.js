export let localServ = {
  setUser: (user) => {
    let dataJson = JSON.stringify(user);
    localStorage.setItem("USER_LOGIN", dataJson);
  },
  getUser: () => {
    let dataJson = localStorage.getItem("USER_LOGIN");
    return JSON.parse(dataJson);
  },
  removeUser: () => {
    localStorage.removeItem("USER_LOGIN");
  },

  setAccessToken: (accessToken) => {
    let dataJson = JSON.stringify(accessToken);
    localStorage.setItem("ACCESS_TOKEN", dataJson);
  },

  getAccessToken: () => {
    let dataJson = localStorage.getItem("ACCESS_TOKEN");
    return JSON.parse(dataJson);
  },

  removeAccessToken: () => {
    localStorage.removeItem("ACCESS_TOKEN");
  },
};
