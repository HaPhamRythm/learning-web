import React from "react";
import { Form, Input, message } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { https } from "../../services/config";
import { useDispatch } from "react-redux";
import { setLogin } from "../../redux/userSlice";
import { localServ } from "../../services/localStoreService";

export default function LoginPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const onFinish = (values) => {
    https
      .post("/api/QuanLyNguoiDung/DangNhap", values)
      .then((res) => {
        message.success("Đăng nhập thành công");

        dispatch(setLogin(res.data));

        localServ.setUser(res.data);
        localServ.setAccessToken(res.data.accessToken);

        setTimeout(() => {
          navigate("/");
        }, 2000);
        console.log(res);
      })
      .catch((err) => {
        message.error("Sai tài khoản và/hoặc mật khẩu");
        console.log(err);
      });
  };
  return (
    <section className="movie-bg">
      <div className="container py-32 flex items-start justify-center relative z-10">
        <div className="w-full sm:w-2/3 md:w-1/2 lg:w-1/3 text-slate-100 bg-slate-900 border border-slate-700 p-6 rounded-md drop-shadow-lg">
          <h2 className="text-xl font-bold text-center mb-4">Đăng nhập</h2>

          <Form onFinish={onFinish} layout="vertical">
            <Form.Item
              label="Tài khoản"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Tài khoản",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Mật khẩu"
              // style={{ color: "white" }}
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Mật khẩu",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item>
              <button
                className="btn btn-primary w-full text-base"
                type="submit"
              >
                Đăng nhập
              </button>
            </Form.Item>
          </Form>

          <div className="text-right">
            <p>
              Bạn chưa có tài khoản? &ndash;{" "}
              <NavLink
                to={"/register"}
                className="text-blue-500 hover:text-blue-600 font-medium"
              >
                Đăng ký
              </NavLink>
            </p>
          </div>
        </div>
      </div>
    </section>
  );
}
