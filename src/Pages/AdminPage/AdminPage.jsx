import React from "react";
import { useState } from "react";
import { https } from "../../services/config";
import { useEffect } from "react";
import { BsWrenchAdjustable } from "react-icons/bs";
import { FaUserEdit } from "react-icons/fa";
import { Tabs, message } from "antd";
import Pagination from "react-paginate";
import ReactPaginate from "react-paginate";
import { localServ } from "../../services/localStoreService";
import { Form, Input, Select } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import "./AdminPage.scss";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
import { AiOutlineEye, AiOutlinePlus, AiOutlineSearch } from "react-icons/ai";
import { useDispatch } from "react-redux";
import {
  setIsAddUser,
  setIsEditUser,
  setIsRegisterUser,
  setPasswordEdit,
  setUserAccessTokenEdit,
  setUserId,
  setUserIdEdit,
} from "../../redux/modalSlice";
import AddUser from "../../Components/Modal/AddUser/AddUser";
import RegisterUser from "../../Components/Modal/RegisterUser/RegisterUser";
import EditUser from "../../Components/Modal/EditUser/EditUser";

export default function AdminPage() {
  const accessToken = localServ.getAccessToken();
  const userAccount = localServ.getUser();
  const dispatch = useDispatch();
  const initialInfo = {
    taiKhoan: "",
    matKhau: "",
    hoTen: "",
    soDT: "",
    maLoaiNguoiDung: "",
    maNhom: "",
    email: "",
  };

  const [editedUser, setEditedUser] = useState(initialInfo);
  // Search name of users

  const [search, setSearch] = useState("");

  // get the list of users and render

  const [userList, setUserList] = useState([]);
  useEffect(() => {
    https
      .get("/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP01")
      .then((res) => {
        // console.log("UserList", res.data);
        setUserList(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  // Pagination
  const postsPerPage = 5;
  const [currentPage, setCurrentPage] = useState(1);
  const currentPosts = userList.filter((user) =>
    user.hoTen.toLowerCase().includes(search)
  );
  const totalItems = currentPosts.length;
  const totalFilteredPages = Math.ceil(totalItems / postsPerPage);
  const startIndex = (currentPage - 1) * postsPerPage;
  const endIndex = startIndex + postsPerPage;
  const currentDisplayedCourses = currentPosts.slice(startIndex, endIndex);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const pageNumberLimit = 5;
  const [minPageNumberLimit, setminPageNumberLimit] = useState(1);
  const [maxPageNumberLimit, setmaxPageNumberLimit] = useState(5);
  const pages = [];
  for (let i = 1; i <= totalFilteredPages; i++) {
    pages.push(i);
  }
  const renderPageNumbers = pages.map((number, index) => {
    if (number < maxPageNumberLimit + 1 && number >= minPageNumberLimit) {
      return (
        <li key={index}>
          <button
            onClick={() => handlePageChange(index + 1)}
            className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 dark:border-gray-700 dark:text-gray-400 ${
              currentPage === index + 1
                ? "dark:bg-gray-700 dark:text-white"
                : "dark:bg-gray-800 hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
            }`}
          >
            {index + 1}
          </button>
        </li>
      );
    }
  });

  const handleNextPage = () => {
    setCurrentPage(currentPage + 1);
    if (currentPage + 1 > maxPageNumberLimit) {
      setmaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
      setminPageNumberLimit(minPageNumberLimit + pageNumberLimit);
    }
  };

  const handlePrePage = () => {
    setCurrentPage(currentPage - 1);
    if ((currentPage - 1) % pageNumberLimit == 0) {
      setmaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
      setminPageNumberLimit(minPageNumberLimit - pageNumberLimit);
    }
  };

  let pageIncrementBtn = null;
  if (pages.length > maxPageNumberLimit) {
    pageIncrementBtn = (
      <button
        onClick={handleNextPage}
        className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 dark:border-gray-700 dark:text-gray-400 
        ? "dark:bg-gray-700 dark:text-white"
        : "dark:bg-gray-800 hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
    }`}
        disabled={currentPage === totalFilteredPages}
      >
        &hellip;
      </button>
    );
  }

  let pageDecrementBtn = null;
  if (pages.length > maxPageNumberLimit) {
    pageDecrementBtn = (
      <button
        onClick={handlePrePage}
        className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 dark:border-gray-700 dark:text-gray-400 
        ? "dark:bg-gray-700 dark:text-white"
        : "dark:bg-gray-800 hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
    }`}
        disabled={currentPage == pages[0] ? true : false}
      >
        &hellip;
      </button>
    );
  }

  // Nut xoa

  let handleDeleteUser = (event) => {
    const userAccount = event.target.getAttribute("data-user-account");

    https
      .delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${userAccount}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((res) => {
        console.log(res);
        message.success("Xoá thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("Không thể xóa vì người dùng đã tạo khóa học!");
      });
  };

  // Button "Sua"

  let handleEditUser = (userIdEdit, passwordEdit) => {
    https
      .post(`/api/QuanLyNguoiDung/DangNhap`, {
        taiKhoan: userIdEdit,
        matKhau: passwordEdit,
      })
      .then((res) => {
        // console.log(res.data);
        dispatch(setIsEditUser(true));
        dispatch(setUserAccessTokenEdit(res.data.accessToken));

        dispatch(setUserIdEdit(userIdEdit));
        dispatch(setPasswordEdit(passwordEdit));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleAddUser = () => {
    dispatch(setIsAddUser(true));
  };

  // Button "Ghi danh"

  const handleRegister = (userId) => {
    dispatch(setIsRegisterUser(true));
    dispatch(setUserId(userId));
  };

  // let renderUserList = () => {
  //   return {
  //     key: "1",
  //     label: (
  //       <h2 className="btn-tab text-sm lg:text-lg lg:uppercase">
  //         Danh sách người dùng
  //       </h2>
  //     ),
  //     children: (
  //       <div className="bg-slate-900 bg-opacity-50 border border-slate-700 p-6 rounded-md drop-shadow-lg">
  //         <div className="relative max-h-96 overflow-y-auto rounded-sm">
  //           <div className="flex items-center justify-around">
  //             <div className="relative w-1/2 mb-4 mr-10 ">
  //               <form>
  //                 <label
  //                   htmlFor="default-search"
  //                   className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
  //                 >
  //                   Search
  //                 </label>
  //                 <div className="relative">
  //                   <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
  //                     <svg
  //                       className="w-4 h-4 text-gray-500 dark:text-gray-400"
  //                       aria-hidden="true"
  //                       xmlns="http://www.w3.org/2000/svg"
  //                       fill="none"
  //                       viewBox="0 0 20 20"
  //                     >
  //                       <path
  //                         stroke="currentColor"
  //                         strokeLinecap="round"
  //                         strokeLinejoin="round"
  //                         strokeWidth={2}
  //                         d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
  //                       />
  //                     </svg>
  //                   </div>
  //                   <input
  //                     type="search"
  //                     id="default-search"
  //                     className="block w-full p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
  //                     placeholder="Tìm theo Họ Tên người dùng"
  //                     required
  //                     onChange={(e) => setSearch(e.target.value)}
  //                   />
  //                 </div>
  //               </form>
  //             </div>
  //             <div className="relative w-1/2 mb-4">
  //               <button
  //                 onClick={handleAddUser}
  //                 type="submit"
  //                 className="inline-flex items-center space-x-1 text-gray-500 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-3 py-1.5 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700 "
  //               >
  //                 <AiOutlinePlus />
  //                 <span>Thêm Người Dùng</span>
  //               </button>
  //             </div>
  //           </div>

  //           <div>
  //             <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 rounded-md">
  //               <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
  //                 <tr>
  //                   <th scope="col" className="p-4">
  //                     STT
  //                   </th>
  //                   <th scope="col" className="px-6 py-3">
  //                     Tài Khoản
  //                   </th>
  //                   <th scope="col" className="px-6 py-3">
  //                     Người Dùng
  //                   </th>
  //                   <th scope="col" className="px-6 py-3">
  //                     Họ Tên
  //                   </th>
  //                   <th scope="col" className="px-6 py-3">
  //                     Email
  //                   </th>
  //                   <th scope="col" className="px-6 py-3">
  //                     Số điện thoại
  //                   </th>
  //                   <th scope="col" className="px-6 py-3">
  //                     <BsWrenchAdjustable />
  //                   </th>
  //                 </tr>
  //               </thead>
  //               <tbody>
  //                 {currentDisplayedCourses.length > 0 ? (
  //                   currentDisplayedCourses.map((user, index) => {
  //                     const absoluteIndex = startIndex + index + 1;
  //                     return (
  //                       <tr
  //                         key={user.taiKhoan}
  //                         className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
  //                       >
  //                         <td
  //                           scope="row"
  //                           className=" w-4 p-4 font-medium text-gray-900 dark:text-white whitespace-nowrap"
  //                         >
  //                           {absoluteIndex}
  //                         </td>
  //                         <td
  //                           scope="row"
  //                           className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
  //                         >
  //                           {user.taiKhoan}
  //                         </td>

  //                         <td
  //                           scope="row"
  //                           className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
  //                         >
  //                           {user.maLoaiNguoiDung}
  //                         </td>

  //                         <td
  //                           scope="row"
  //                           className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
  //                         >
  //                           {user.hoTen}
  //                         </td>

  //                         <td
  //                           scope="row"
  //                           className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
  //                         >
  //                           {user.email}
  //                         </td>

  //                         <td
  //                           scope="row"
  //                           className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
  //                         >
  //                           {user.soDt}
  //                         </td>

  //                         <td
  //                           scope="row"
  //                           className="space-x-1 px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
  //                         >
  //                           <button
  //                             className="font-medium text-green-700 dark:text-green-400 hover:underline"
  //                             onClick={() => handleRegister(user.taiKhoan)}
  //                           >
  //                             Ghi danh
  //                           </button>
  //                           <span>|</span>
  //                           <button
  //                             className="font-medium text-sky-600 dark:text-sky-500 hover:underline"
  //                             data-bs-toggle="modal"
  //                             data-bs-target="#myModal"
  //                             onClick={() =>
  //                               handleEditUser(user.taiKhoan, user.matKhau)
  //                             }
  //                           >
  //                             Sửa
  //                           </button>
  //                           <span>|</span>
  //                           <button
  //                             className="font-medium text-red-600 dark:text-red-500 hover:underline"
  //                             onClick={handleDeleteUser}
  //                             data-user-account={user.taiKhoan}
  //                             data-user-password={user.matKhau}
  //                           >
  //                             Xóa
  //                           </button>
  //                         </td>
  //                       </tr>
  //                     );
  //                   })
  //                 ) : (
  //                   <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
  //                     <td className="w-4 p-4 text-center" colSpan={6}>
  //                       Không tìm thấy khóa học
  //                     </td>
  //                   </tr>
  //                 )}
  //               </tbody>
  //             </table>
  //           </div>
  //         </div>

  //         {/* pagination */}
  //         {totalFilteredPages > 1 ? (
  //           <nav
  //             className="w-full flex items-center justify-end pt-4"
  //             aria-label="Table navigation"
  //           >
  //             <ul className="inline-flex -space-x-px text-sm h-8">
  //               <li>
  //                 <button
  //                   onClick={handlePrePage}
  //                   className={`flex items-center justify-center px-3 h-8 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-l-lg dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
  //                     currentPage === 1
  //                       ? "cursor-not-allowed"
  //                       : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
  //                   }`}
  //                   disabled={currentPage == pages[0] ? true : false}
  //                 >
  //                   <BsChevronLeft />
  //                 </button>
  //               </li>
  //               {pageDecrementBtn}
  //               {renderPageNumbers}
  //               {pageIncrementBtn}
  //               <li>
  //                 <button
  //                   onClick={() => handlePageChange(totalFilteredPages)}
  //                   className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300  dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
  //                     currentPage === totalFilteredPages
  //                       ? "cursor-not-allowed"
  //                       : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
  //                   }`}
  //                   disabled={currentPage === totalFilteredPages}
  //                 >
  //                   {totalFilteredPages}
  //                 </button>
  //               </li>
  //               <li>
  //                 <button
  //                   onClick={handleNextPage}
  //                   className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
  //                     currentPage === totalFilteredPages
  //                       ? "cursor-not-allowed"
  //                       : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
  //                   }`}
  //                   disabled={currentPage === totalFilteredPages}
  //                 >
  //                   <BsChevronRight />
  //                 </button>
  //               </li>
  //             </ul>
  //           </nav>
  //         ) : null}
  //       </div>
  //     ),
  //   };
  // };
  // const items = [renderUserList()];

  return (
    <section className="movie-bg">
      {/* <div className="container py-24 relative z-10">
        <Tabs defaultActiveKey="1" className="movie-tab" items={items} />
      </div> */}
      <div className="container py-24 relative z-10">
        <div className="bg-slate-300 bg-opacity-50 border border-slate-200 p-6 rounded-md drop-shadow-lg">
          <div className="relative max-h-96 overflow-y-auto rounded-sm">
            <div className="flex items-center justify-around">
              <div className="relative w-1/2 mb-4 mr-10 ">
                <form>
                  <label
                    htmlFor="default-search"
                    className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
                  >
                    Search
                  </label>
                  <div className="relative">
                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                      <svg
                        className="w-4 h-4 text-gray-500 dark:text-gray-400"
                        aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 20 20"
                      >
                        <path
                          stroke="currentColor"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={2}
                          d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                        />
                      </svg>
                    </div>
                    <input
                      type="search"
                      id="default-search"
                      className="block w-full p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Tìm theo Họ Tên người dùng"
                      required
                      onChange={(e) => setSearch(e.target.value)}
                    />
                  </div>
                </form>
              </div>
              <div className="relative w-1/2 mb-4">
                <button
                  onClick={handleAddUser}
                  type="submit"
                  className="inline-flex items-center space-x-1 text-gray-500 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-3 py-1.5 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700 "
                >
                  <AiOutlinePlus />
                  <span>Thêm Người Dùng</span>
                </button>
              </div>
            </div>

            <div>
              <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400 rounded-md">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                  <tr>
                    <th scope="col" className="p-4">
                      STT
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Tài Khoản
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Người Dùng
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Họ Tên
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Email
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Số điện thoại
                    </th>
                    <th scope="col" className="px-6 py-3">
                      <BsWrenchAdjustable />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {currentDisplayedCourses.length > 0 ? (
                    currentDisplayedCourses.map((user, index) => {
                      const absoluteIndex = startIndex + index + 1;
                      return (
                        <tr
                          key={user.taiKhoan}
                          className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                        >
                          <td
                            scope="row"
                            className=" w-4 p-4 font-medium text-gray-900 dark:text-white whitespace-nowrap"
                          >
                            {absoluteIndex}
                          </td>
                          <td
                            scope="row"
                            className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
                          >
                            {user.taiKhoan}
                          </td>

                          <td
                            scope="row"
                            className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
                          >
                            {user.maLoaiNguoiDung}
                          </td>

                          <td
                            scope="row"
                            className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
                          >
                            {user.hoTen}
                          </td>

                          <td
                            scope="row"
                            className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
                          >
                            {user.email}
                          </td>

                          <td
                            scope="row"
                            className="px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
                          >
                            {user.soDt}
                          </td>

                          <td
                            scope="row"
                            className="space-x-1 px-4 py-2 font-semibold text-gray-900 dark:text-white whitespace-nowrap"
                          >
                            <button
                              className="font-medium text-green-700 dark:text-green-400 hover:underline"
                              onClick={() => handleRegister(user.taiKhoan)}
                            >
                              Ghi danh
                            </button>
                            <span>|</span>
                            <button
                              className="font-medium text-sky-600 dark:text-sky-500 hover:underline"
                              data-bs-toggle="modal"
                              data-bs-target="#myModal"
                              onClick={() =>
                                handleEditUser(user.taiKhoan, user.matKhau)
                              }
                            >
                              Sửa
                            </button>
                            <span>|</span>
                            <button
                              className="font-medium text-red-600 dark:text-red-500 hover:underline"
                              onClick={handleDeleteUser}
                              data-user-account={user.taiKhoan}
                              data-user-password={user.matKhau}
                            >
                              Xóa
                            </button>
                          </td>
                        </tr>
                      );
                    })
                  ) : (
                    <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                      <td className="w-4 p-4 text-center" colSpan={6}>
                        Không tìm thấy khóa học
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>

          {/* pagination */}
          {totalFilteredPages > 1 ? (
            <nav
              className="w-full flex items-center justify-end pt-4"
              aria-label="Table navigation"
            >
              <ul className="inline-flex -space-x-px text-sm h-8">
                <li>
                  <button
                    onClick={handlePrePage}
                    className={`flex items-center justify-center px-3 h-8 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-l-lg dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
                      currentPage === 1
                        ? "cursor-not-allowed"
                        : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                    }`}
                    disabled={currentPage == pages[0] ? true : false}
                  >
                    <BsChevronLeft />
                  </button>
                </li>
                {pageDecrementBtn}
                {renderPageNumbers}
                {pageIncrementBtn}
                <li>
                  <button
                    onClick={() => handlePageChange(totalFilteredPages)}
                    className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300  dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
                      currentPage === totalFilteredPages
                        ? "cursor-not-allowed"
                        : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                    }`}
                    disabled={currentPage === totalFilteredPages}
                  >
                    {totalFilteredPages}
                  </button>
                </li>
                <li>
                  <button
                    onClick={handleNextPage}
                    className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
                      currentPage === totalFilteredPages
                        ? "cursor-not-allowed"
                        : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                    }`}
                    disabled={currentPage === totalFilteredPages}
                  >
                    <BsChevronRight />
                  </button>
                </li>
              </ul>
            </nav>
          ) : null}
        </div>
      </div>

      {/* admin's form to edit users */}
      <EditUser />

      {/* muốn xài modal ở đâu thì phải gọi modal ở đó !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */}
      <AddUser />

      <RegisterUser />

      {/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */}
    </section>
  );
}
