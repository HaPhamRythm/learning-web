import React from "react";
import { https } from "../../services/config";
import { localServ } from "../../services/localStoreService";
import { useEffect } from "react";
import { useState } from "react";
import { Tabs, message } from "antd";
import moment from "moment/moment";
import "./AccountPage.scss";

export default function AccountPage() {
  const userLogin = localServ.getUser();

  const accessToken = localServ.getAccessToken();

  const [accountInfo, setAccountInfo] = useState({
    email: "",
    hoTen: "",
    maLoaiNguoiDung: "",
    maNhom: "",
    matKhau: "",
    soDT: "",
    taiKhoan: "",
    chiTietKhoaHocGhiDanh: [],
  });

  useEffect(() => {
    https
      .post(
        `/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
        {},
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      )
      .then((res) => {
        // console.log(res);
        setAccountInfo((preAccountInfo) => {
          return Object.assign({}, preAccountInfo, res.data);
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  // console.log(accountInfo);

  const handleSubmit = (event) => {
    event.preventDefault();
    https
      .put("/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", accountInfo, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((res) => {
        console.log(res.data);
        message.success("Cập nhật thành công");
        localServ.setUser(res.data);
        window.location.reload();
      })
      .catch((err) => {
        message.err("Chưa cập nhật thành công");
        console.log(err);
      });
  };

  let renderAccountForm = () => {
    return {
      key: "1",
      label: (
        <h2 className="btn-tab text-sm lg:text-lg lg:uppercase">
          Thông tin tài khoản
        </h2>
      ),
      children: (
        <div className="flex items-start space-x-0 lg:space-x-10 ">
          <div className="form-visual hidden lg:block lg:w-1/4 ">
            <img
              src={`https://ui-avatars.com/api/?background=random&name=${accountInfo.hoTen}`}
              alt={accountInfo.hoTen}
              className="w-1/2 rounded-full border-2 border-white mx-auto"
            />
            <h3 className="text-xl font-medium text-center text-slate-200 mt-4">
              {accountInfo.taiKhoan}
            </h3>
          </div>

          <div className="form-visual w-full lg:w-3/4 ">
            <form onSubmit={handleSubmit}>
              <div className="mb-6 block lg:hidden ">
                <img
                  src={`https://ui-avatars.com/api/?background=random&name=${accountInfo.hoTen}`}
                  alt={accountInfo.hoTen}
                  className="w-8 md:w-12 rounded-full border-2 border-white mx-auto"
                />
                <h3 className="text-lg font-medium text-center text-slate-100 mt-2">
                  {accountInfo.taiKhoan}
                </h3>
              </div>
              <div className="grid gap-4 mb-4 sm:grid-cols-2 ">
                <div className="mb-6 hidden">
                  <label
                    htmlFor="taiKhoan"
                    className="block mb-2 text-sm font-medium text-slate-900 dark:text-slate-100"
                  >
                    Tài khoản
                  </label>
                  <input
                    value={accountInfo.taiKhoan}
                    type="text"
                    id="taiKhoan"
                    className="movie-input cursor-not-allowed"
                    required
                    disabled
                  />
                </div>
                <div className="mb-6">
                  <label
                    htmlFor="matKhau"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                  >
                    Mật khẩu
                  </label>
                  <input
                    // get matKhau in api to form
                    value={accountInfo.matKhau}
                    onChange={(e) =>
                      setAccountInfo((prevState) => ({
                        ...prevState,
                        matKhau: e.target.value,
                      }))
                    }
                    type="password"
                    id="matKhau"
                    className="movie-input"
                    required
                  />
                </div>
                <div className="mb-6">
                  <label
                    htmlFor="hoTen"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                  >
                    Họ tên
                  </label>
                  <input
                    value={accountInfo.hoTen}
                    onChange={(e) =>
                      setAccountInfo((prevState) => ({
                        ...prevState,
                        hoTen: e.target.value,
                      }))
                    }
                    type="text"
                    id="hoTen"
                    className="movie-input"
                    placeholder="Vd: PBT MOVIE"
                    required
                  />
                </div>
                <div className="mb-6">
                  <label
                    htmlFor="email"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                  >
                    Email
                  </label>
                  <input
                    value={accountInfo.email}
                    onChange={(e) =>
                      setAccountInfo((prevState) => ({
                        ...prevState,
                        email: e.target.value,
                      }))
                    }
                    type="email"
                    id="email"
                    className="movie-input"
                    placeholder="Vd: email@pbtmovie.com"
                    required
                  />
                </div>
                <div className="mb-6">
                  <label
                    htmlFor="soDT"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                  >
                    Số điện thoại
                  </label>
                  <input
                    value={accountInfo.soDT}
                    onChange={(e) =>
                      setAccountInfo((prevState) => ({
                        ...prevState,
                        soDT: e.target.value,
                      }))
                    }
                    type="text"
                    id="soDT"
                    className="movie-input"
                    placeholder="Ví dụ: 01234567"
                    required
                  />
                </div>
                <div>
                  <button type="submit" className="btn btn-primary w-1/2">
                    Cập nhật
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      ),
    };
  };

  let renderRegisteredCourses = () => {
    return {
      key: "2",
      label: (
        <h2 className="btn-tab text-sm lg:text-lg lg:uppercase">
          Các khoá học đã đăng ký
        </h2>
      ),
      children: (
        <div className="bg-slate-900 bg-opacity-50 border border-slate-700 p-6 rounded-md drop-shadow-lg">
          <div className="relative max-h-96 overflow-y-auto rounded-sm">
            <table className="w-full text-xs md:text-sm text-left text-gray-500 dark:text-gray-400">
              <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Tên khoá học
                  </th>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Lượt xem
                  </th>
                  <th scope="col" className="px-3 py-1.5 lg:px-6 lg:py-3">
                    Ngày đặt
                  </th>
                </tr>
              </thead>
              <tbody>
                {accountInfo.chiTietKhoaHocGhiDanh.length ? (
                  accountInfo.chiTietKhoaHocGhiDanh.map((course) => {
                    return (
                      <tr
                        key={course.maKhoaHoc}
                        className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
                      >
                        <td
                          scope="row"
                          className="px-3 py-2 lg:px-6 lg:-py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap"
                        >
                          {course.tenKhoaHoc}
                        </td>

                        <td
                          scope="row"
                          className="px-3 py-2 lg:px-6 lg:-py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap"
                        >
                          {course.luotXem}
                        </td>

                        <td className="px-3 py-2 lg:px-6 lg:-py-4">
                          {moment(course.ngayTao).format("hh:mm - DD.MM.YYYY")}
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                    <td colSpan={3} className="px-3 py-2 lg:px-6 lg:-py-4">
                      <p>
                        Chưa có khoá học đăng ký, vui lòng tham khảo các khoá
                        học để đăng ký.
                      </p>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      ),
    };
  };

  const items = [renderAccountForm(), renderRegisteredCourses()];
  return (
    <section className="section p-account text-gray-700 bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
      <div className="container py-24 relative z-10">
        <Tabs defaultActiveKey="1" className="movie-tab" items={items} />
      </div>
      {/* <div className="container">
        <div className="section-header">
          <div className="section-title">Thông tin Tài Khoản</div>
          <div className="section-devider">
            <div className="section-devider__stroke"></div>
          </div>
        </div>

        <div className="p-account__row">
          <div className="p-account__col">
            <div className="p-member__form p-account__form">

            </div>
          </div>
          <div className="p-account__col"></div>
        </div>
      </div> */}
    </section>
  );
}
