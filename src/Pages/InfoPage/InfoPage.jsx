import React from "react";
import { FaCheckCircle } from "react-icons/fa";

export default function InfoPage() {
  return (
    <div className="About">
      {/* carousel */}

      <div className="sliderAbout">
        <div className="sliderContentAbout">
          <p className="titleSliderAbout text-xl text-white">
            V learning học là vui{" "}
          </p>
          <h6 className="my-5">Cùng nhau khám phá nhưng điều mới mẻ</h6>
          <p>Học đi đôi với hành</p>
        </div>
      </div>

      {/* items */}

      <div className="AboutItem ">
        <div className="row flex justify-center items-center">
          <div
            className="w-1/3 aboutUs wow animate__animated animate__fadeInLeft"
            data-wow-duration="4s"
            data-wow-delay="0.2s"
          >
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQt_ipZVtlpChSqwer21Ahvdb9O-WsxEiTeOA&usqp=CAU"
              alt
            />
          </div>
          <div
            className="w-2/3 aboutUs wow animate__animated animate__fadeInRight"
            data-wow-duration="4s"
            data-wow-delay="0.2s"
          >
            <div className="title">
              <h3>V LEARNING ?</h3>
              <h2>Tại sao chọn học tại Trung tâm?</h2>
            </div>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita
              ipsum, id ut optio quas amet obcaecati consequuntur nulla facere
              fuga.
            </p>

            <div className="row  flex items-start justify-left">
              <FaCheckCircle
                className="mr-3"
                style={{ color: "#57cc99", fontSize: "20px" }}
              />
              Nằm trong top các trường đại học hàng đầu thế giới, các trường đại
              học ở Anh có uy tín trong việc đào tạo cho sinh viên những kỹ năng
              mà các nhà tuyển dụng tìm kiếm.
            </div>
            <div className="row  flex items-start justify-left">
              <FaCheckCircle
                className="mr-3"
                style={{ color: "#57cc99", fontSize: "20px" }}
              />
              Ngoài các giờ học trên giảng đường, bạn còn có cơ hội tương tác
              trực tiếp với giáo viên thông qua các tiết học bồi dưỡng, buổi
              thảo luận sinh động và các dự án thực tiễn, giúp nâng cao khả năng
              tư duy sắc bén, sự sáng tạo và tự tin.
            </div>
            <div className="row  flex items-start justify-left">
              <FaCheckCircle
                className="mr-3"
                style={{ color: "#57cc99", fontSize: "20px" }}
              />
              Tiếp cận với các trang thiết bị tân tiến nhất như cơ sở vật chất
              rộng rãi, thoáng mát, trang thiết bị học tập hiện đại.
            </div>
            <div className="row  flex items-center justify-left">
              <FaCheckCircle
                className="mr-3"
                style={{ color: "#57cc99", fontSize: "20px" }}
              />
              Có liên kết với các trường đại học danh tiếng tại Châu Âu, Châu
              Mỹ, Canada.
            </div>
          </div>
        </div>
      </div>

      <div className="AboutItem">
        <div className="row  flex justify-center items-center">
          <div
            className="w-2/3 aboutUs wow animate__animated animate__fadeInRight"
            data-wow-duration="4s"
            data-wow-delay="4s"
            data-wow-iteration="3"
          >
            <div className="title">
              <h3>CHƯƠNG TRÌNH HỌC V LEARNING</h3>
              <h2>Hệ thống học hàng đầu</h2>
            </div>
            <p>
              Giảng viên đều là các chuyên viên thiết kế đồ họa và giảng viên
              của các trường đại học danh tiếng trong thành phố: Đại học CNTT,
              KHTN, Bách Khoa,…Trang thiết bị phục vụ học tập đầy đủ, phòng học
              máy lạnh, màn hình LCD, máy cấu hình mạnh, mỗi học viên thực hành
              trên một máy riêng.100% các buổi học đều là thực hành trên máy
              tính. Đào tạo với tiêu chí: “học để làm được việc”, mang lại cho
              học viên những kiến thức hữu ích nhất, sát với thực tế nhất.
            </p>
          </div>
          <div
            className="w-1/3 aboutUs wow animate__animated animate__fadeInLeft"
            data-wow-duration="4s"
            data-wow-delay="10s"
          >
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiO4M8RHyUkeJU5mtQnlz1glY3v3UVJg1hdQ&usqp=CAU"
              alt
            />
          </div>
        </div>
      </div>

      <div className="AboutItem ">
        <div className="row flex justify-center items-center">
          <div
            className="w-1/3 aboutUs wow animate__animated animate__fadeInLeft"
            data-wow-duration="4s"
            data-wow-delay="0.2s"
          >
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQt_ipZVtlpChSqwer21Ahvdb9O-WsxEiTeOA&usqp=CAU"
              alt
            />
          </div>
          <div
            className="w-2/3 aboutUs wow animate__animated animate__fadeInRight"
            data-wow-duration="4s"
            data-wow-delay="0.2s"
          >
            <div className="title">
              <h3>TẦM NHÌN V LEARNING</h3>
              <h2>Đào tạo lập trình chuyên sâu</h2>
            </div>
            <p>
              Trở thành hệ thống đào tạo lập trình chuyên sâu theo nghề hàng đầu
              khu vực, cung cấp nhân lực có tay nghề cao, chuyên môn sâu cho sự
              phát triển công nghiệp phần mềm trong thời đại công nghệ số hiện
              nay, góp phần giúp sự phát triển của xã hội, đưa Việt Nam thành
              cường quốc về phát triển phần mềm.giúp người học phát triển cả tư
              duy, phân tích, chuyên sâu nghề nghiệp, học tập suốt đời, sẵn sàng
              đáp ứng mọi nhu cầu của doanh nghiệp.
            </p>
          </div>
        </div>
      </div>

      <div className="AboutItem">
        <div className="row  flex justify-center items-center">
          <div
            className="w-2/3 aboutUs wow animate__animated animate__fadeInRight"
            data-wow-duration="4s"
            data-wow-delay="10s"
          >
            <div className="title">
              <h3>SỨ MỆNH V LEARNING</h3>
              <h2>Phương pháp đào tạo hiện đại</h2>
            </div>
            <p>
              Sử dụng các phương pháp đào tạo tiên tiến và hiện đại trên nền
              tảng công nghệ giáo dục, kết hợp phương pháp truyền thống, phương
              pháp trực tuyến, lớp học đảo ngược và học tập dựa trên dự án thực
              tế, phối hợp giữa đội ngũ training nhiều kinh nghiệm và yêu cầu từ
              các công ty, doanh nghiệp. Qua đó, V learning giúp người học phát
              triển cả tư duy, phân tích, chuyên sâu nghề nghiệp, học tập suốt
              đời, sẵn sàng đáp ứng mọi nhu cầu của doanh nghiệp.
            </p>
          </div>
          <div
            className="w-1/3 aboutUs wow animate__animated animate__fadeInLeft"
            data-wow-duration="4s"
            data-wow-delay="10s"
          >
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiO4M8RHyUkeJU5mtQnlz1glY3v3UVJg1hdQ&usqp=CAU"
              alt
            />
          </div>
        </div>
      </div>
    </div>

    // <div className="About">
    //   <div
    //     id="default-carousel"
    //     className=" relative w-full"
    //     data-carousel="slide"
    //   >
    //     {/* Carousel wrapper */}
    //     <div className="relative h-́80 overflow-hidden rounded-lg md:h-96">
    //       {/* Item 1 */}
    //       <div
    //         className=" duration-700 ease-in-out bg-no-repeat bg-center bg-full"
    //         data-carousel-item
    //       >
    //         <img
    //           src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnhT240ED6oOXmfj_di-_JD2y8N-bEY2RMyw&usqp=CAU"
    //           className="absolute block w-full h-screen -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
    //           alt="..."
    //           // style={{
    //           //   backgroundSize: "cover",
    //           //   backgroundPosition: "center",
    //           //   backgroundRepeat: "no-repeat",
    //           // }}
    //         />
    //       </div>
    //       {/* Item 2 */}
    //       <div className="hidden duration-700 ease-in-out" data-carousel-item>
    //         <img
    //           src="/docs/images/carousel/carousel-2.svg"
    //           className="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
    //           alt="..."
    //         />
    //       </div>
    //       {/* Item 3 */}
    //       <div className="hidden duration-700 ease-in-out" data-carousel-item>
    //         <img
    //           src="/docs/images/carousel/carousel-3.svg"
    //           className="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
    //           alt="..."
    //         />
    //       </div>
    //       {/* Item 4 */}
    //       <div className="hidden duration-700 ease-in-out" data-carousel-item>
    //         <img
    //           src="/docs/images/carousel/carousel-4.svg"
    //           className="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
    //           alt="..."
    //         />
    //       </div>
    //       {/* Item 5 */}
    //       <div className="hidden duration-700 ease-in-out" data-carousel-item>
    //         <img
    //           src="/docs/images/carousel/carousel-5.svg"
    //           className="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
    //           alt="..."
    //         />
    //       </div>
    //     </div>
    //     {/* Slider indicators */}
    //     <div className="absolute z-30 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
    //       <button
    //         type="button"
    //         className="w-3 h-3 rounded-full"
    //         aria-current="true"
    //         aria-label="Slide 1"
    //         data-carousel-slide-to={0}
    //       />
    //       <button
    //         type="button"
    //         className="w-3 h-3 rounded-full"
    //         aria-current="false"
    //         aria-label="Slide 2"
    //         data-carousel-slide-to={1}
    //       />
    //       <button
    //         type="button"
    //         className="w-3 h-3 rounded-full"
    //         aria-current="false"
    //         aria-label="Slide 3"
    //         data-carousel-slide-to={2}
    //       />
    //       <button
    //         type="button"
    //         className="w-3 h-3 rounded-full"
    //         aria-current="false"
    //         aria-label="Slide 4"
    //         data-carousel-slide-to={3}
    //       />
    //       <button
    //         type="button"
    //         className="w-3 h-3 rounded-full"
    //         aria-current="false"
    //         aria-label="Slide 5"
    //         data-carousel-slide-to={4}
    //       />
    //     </div>
    //     {/* Slider controls */}
    //     <button
    //       type="button"
    //       className="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
    //       data-carousel-prev
    //     >
    //       <span className="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
    //         <svg
    //           className="w-4 h-4 text-white dark:text-gray-800"
    //           aria-hidden="true"
    //           xmlns="http://www.w3.org/2000/svg"
    //           fill="none"
    //           viewBox="0 0 6 10"
    //         >
    //           <path
    //             stroke="currentColor"
    //             strokeLinecap="round"
    //             strokeLinejoin="round"
    //             strokeWidth={2}
    //             d="M5 1 1 5l4 4"
    //           />
    //         </svg>
    //         <span className="sr-only">Previous</span>
    //       </span>
    //     </button>
    //     <button
    //       type="button"
    //       className="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
    //       data-carousel-next
    //     >
    //       <span className="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
    //         <svg
    //           className="w-4 h-4 text-white dark:text-gray-800"
    //           aria-hidden="true"
    //           xmlns="http://www.w3.org/2000/svg"
    //           fill="none"
    //           viewBox="0 0 6 10"
    //         >
    //           <path
    //             stroke="currentColor"
    //             strokeLinecap="round"
    //             strokeLinejoin="round"
    //             strokeWidth={2}
    //             d="m1 9 4-4-4-4"
    //           />
    //         </svg>
    //         <span className="sr-only">Next</span>
    //       </span>
    //     </button>
    //   </div>
    // </div>
  );
}
