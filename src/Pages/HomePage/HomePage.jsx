import React from "react";
import HeroSection from "./HeroSection/HeroSection";
import Statistic from "./Statistic/Statistic";
import NewCourse from "./NewCourse/NewCourse";
import StudySystem from "./StudySystem/StudySystem";
import OurTeam from "./OurTeam/OurTeam";
import { scrollToTop } from "../../services/effect";
import { useEffect } from "react";

export default function HomePage() {
  useEffect(() => {
    scrollToTop();
  }, []);

  return (
    <>
      <HeroSection />
      <Statistic />
      <NewCourse />
      <StudySystem />
      <OurTeam />
    </>
  );
}
