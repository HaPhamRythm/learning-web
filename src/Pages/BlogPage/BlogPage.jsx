import React from "react";
import { FaRegThumbsUp, FaMessage, FaEye, FaRegMessage } from "react-icons/fa";
import { BsFillChatDotsFill, BsFillChatFill } from "react-icons/bs";
import style from "./style.css";

export default function BlogPage() {
  return (
    <div className="blogCourseContainer container">
      <h6>
        <a href="">
          <i class="fas fa-bullhorn"></i>
          PHÙ HỢP VỚI BẠN
        </a>
      </h6>

      <div class="row flex">
        <div className="w-2/3">
          <div className="row flex space-x-5 m-5 items-center justify-center">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="image rounded-t-lg"
                  src="https://tricks4english.files.wordpress.com/2013/11/fun_to_learn_sticker.jpg"
                  alt
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Learning is fun
                  </h5>
                </a>
                <div className="row flex items-center justify-start space-x-3">
                  <FaRegThumbsUp color="#00b4d8" /> 7k
                  <FaEye color="#00b4d8" /> 1M{" "}
                  <BsFillChatFill color="#00b4d8" /> 1k
                </div>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Here are the biggest enterprise technology acquisitions of
                  2021 so far, in reverse chronological order.
                </p>
                <a
                  href="#"
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Read more
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                </a>
              </div>
            </div>
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://static01.nyt.com/images/2015/03/20/education/20EDU/20EDU-superJumbo.jpg"
                  alt
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Noteworthy technology acquisitions 2021
                  </h5>
                </a>
                <div className="row flex items-center justify-start space-x-3">
                  <FaRegThumbsUp color="#00b4d8" /> 7k
                  <FaEye color="#00b4d8" /> 1M{" "}
                  <BsFillChatFill color="#00b4d8" /> 1k
                </div>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Here are the biggest enterprise technology acquisitions of
                  2021 so far, in reverse chronological order.
                </p>
                <a
                  href="#"
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Read more
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>

          <div className="row flex space-x-5 m-5 items-center justify-center">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://i.ytimg.com/vi/zdfhzwQPxSc/maxresdefault.jpg"
                  alt
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Smart learning
                  </h5>
                </a>
                <div className="row flex items-center justify-start space-x-3">
                  <FaRegThumbsUp color="#00b4d8" /> 7k
                  <FaEye color="#00b4d8" /> 1M{" "}
                  <BsFillChatFill color="#00b4d8" /> 1k
                </div>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Here are the biggest enterprise technology acquisitions of
                  2021 so far, in reverse chronological order.
                </p>
                <a
                  href="#"
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Read more
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                </a>
              </div>
            </div>
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://m.media-amazon.com/images/I/71ocH0mfngL._AC_UF1000,1000_QL80_.jpg"
                  alt
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Noteworthy technology acquisitions 2021
                  </h5>
                </a>
                <div className="row flex items-center justify-start space-x-3">
                  <FaRegThumbsUp color="#00b4d8" /> 7k
                  <FaEye color="#00b4d8" /> 1M{" "}
                  <BsFillChatFill color="#00b4d8" /> 1k
                </div>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Here are the biggest enterprise technology acquisitions of
                  2021 so far, in reverse chronological order.
                </p>
                <a
                  href="#"
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Read more
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>

          <div className="row flex space-x-5 m-5 items-center justify-center">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://i.ytimg.com/vi/zdfhzwQPxSc/maxresdefault.jpg"
                  alt
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Smart learning
                  </h5>
                </a>
                <div className="row flex items-center justify-start space-x-3">
                  <FaRegThumbsUp color="#00b4d8" /> 7k
                  <FaEye color="#00b4d8" /> 1M{" "}
                  <BsFillChatFill color="#00b4d8" /> 1k
                </div>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Here are the biggest enterprise technology acquisitions of
                  2021 so far, in reverse chronological order.
                </p>
                <a
                  href="#"
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Read more
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                </a>
              </div>
            </div>
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://m.media-amazon.com/images/I/71ocH0mfngL._AC_UF1000,1000_QL80_.jpg"
                  alt
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Noteworthy technology acquisitions 2021
                  </h5>
                </a>
                <div className="row flex items-center justify-start space-x-3">
                  <FaRegThumbsUp color="#00b4d8" /> 7k
                  <FaEye color="#00b4d8" /> 1M{" "}
                  <BsFillChatFill color="#00b4d8" /> 1k
                </div>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Here are the biggest enterprise technology acquisitions of
                  2021 so far, in reverse chronological order.
                </p>
                <a
                  href="#"
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Read more
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>

          <div className="row flex space-x-5 m-5 items-center justify-center">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://i.ytimg.com/vi/zdfhzwQPxSc/maxresdefault.jpg"
                  alt
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Smart learning
                  </h5>
                </a>
                <div className="row flex items-center justify-start space-x-3">
                  <FaRegThumbsUp color="#00b4d8" /> 7k
                  <FaEye color="#00b4d8" /> 1M{" "}
                  <BsFillChatFill color="#00b4d8" /> 1k
                </div>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Here are the biggest enterprise technology acquisitions of
                  2021 so far, in reverse chronological order.
                </p>
                <a
                  href="#"
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Read more
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                </a>
              </div>
            </div>
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <a href="#">
                <img
                  className="rounded-t-lg"
                  src="https://m.media-amazon.com/images/I/71ocH0mfngL._AC_UF1000,1000_QL80_.jpg"
                  alt
                />
              </a>
              <div className="p-5">
                <a href="#">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Noteworthy technology acquisitions 2021
                  </h5>
                </a>
                <div className="row flex items-center justify-start space-x-3">
                  <FaRegThumbsUp color="#00b4d8" /> 7k
                  <FaEye color="#00b4d8" /> 1M{" "}
                  <BsFillChatFill color="#00b4d8" /> 1k
                </div>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Here are the biggest enterprise technology acquisitions of
                  2021 so far, in reverse chronological order.
                </p>
                <a
                  href="#"
                  className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  Read more
                  <svg
                    className="w-3.5 h-3.5 ml-2"
                    aria-hidden="true"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 14 10"
                  >
                    <path
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M1 5h12m0 0L9 1m4 4L9 9"
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>
        </div>

        <div className="blogItemRight w-1/3 flex flex-col m-5">
          <div className="blogRightBox m-5 border border-gray-200 rounded-lg shadow-sm dark:border-gray-700">
            <h6 className="border border-gray-200 p-3 shadow-sm dark:border-gray-700 text-lg font-semibold text-gray-900">
              Chủ đề được tìm kiếm nhiều nhất
            </h6>
            <ul className="p-3 space-y-3">
              <li>
                <a className="hover:text-blue-400" href="">
                  Front-end / Mobile apps
                </a>
              </li>

              <li>
                <a className="hover:text-blue-400" href="">
                  UI / UX / Design
                </a>
              </li>
              <li>
                <a className="hover:text-blue-400" href="">
                  BACK-END
                </a>
              </li>
              <li>
                <a className="hover:text-blue-400" href="">
                  Thư viện
                </a>
              </li>
              <li>
                <a className="hover:text-blue-400" href="">
                  Chia sẻ người trong nghề
                </a>
              </li>

              <li>
                <a className="hover:text-blue-400" href="">
                  Châm ngôn IT
                </a>
              </li>
              <li>
                <a className="hover:text-blue-400" href="">
                  Chủ đề khác
                </a>
              </li>
            </ul>
          </div>

          <div className="blogRightBox ">
            <div className="grid m-3 border border-gray-200 rounded-lg shadow-sm dark:border-gray-700 md:mb-12 md:grid-cols-1">
              <h6 className="border border-gray-200 p-3 shadow-sm dark:border-gray-700 text-lg font-semibold text-gray-900">
                Bài đăng được xem nhiều nhất
              </h6>
              <figure className="flex flex-col items-center justify-center p-3 text-center bg-white border-b border-gray-200 rounded-t-lg md:rounded-t-none md:rounded-tl-lg md:border-r dark:bg-gray-800 dark:border-gray-700">
                <blockquote className="max-w-xl mx-auto mb-1 text-gray-500 lg:mb-8 dark:text-gray-400">
                  <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                    Very easy this was to integrate
                  </h3>
                  <p className="my-4">
                    If you care for your time, I hands down would go with this."
                  </p>
                </blockquote>
                <figcaption className="flex items-center justify-center space-x-3">
                  <img
                    className="rounded-full w-9 h-9"
                    src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/avatars/karen-nelson.png"
                    alt="profile picture"
                  />
                  <div className="space-y-0.5 font-medium dark:text-white text-left">
                    <div>Bonnie Green</div>
                    <div className="text-sm text-gray-500 dark:text-gray-400">
                      Developer at Open AI
                    </div>
                  </div>
                </figcaption>
              </figure>
              <figure className="flex flex-col items-center justify-center p-8 text-center bg-white border-b border-gray-200 rounded-tr-lg dark:bg-gray-800 dark:border-gray-700">
                <blockquote className="max-w-2xl mx-auto mb-4 text-gray-500 lg:mb-8 dark:text-gray-400">
                  <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                    Solid foundation for any project
                  </h3>
                  <p className="my-4">
                    Designing with Figma components that can be easily
                    translated to the utility classes of Tailwind CSS is a huge
                    timesaver!"
                  </p>
                </blockquote>
                <figcaption className="flex items-center justify-center space-x-3">
                  <img
                    className="rounded-full w-9 h-9"
                    src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/avatars/roberta-casas.png"
                    alt="profile picture"
                  />
                  <div className="space-y-0.5 font-medium dark:text-white text-left">
                    <div>Roberta Casas</div>
                    <div className="text-sm text-gray-500 dark:text-gray-400">
                      Lead designer at Dropbox
                    </div>
                  </div>
                </figcaption>
              </figure>
              <figure className="flex flex-col items-center justify-center p-8 text-center bg-white border-b border-gray-200 rounded-bl-lg md:border-b-0 md:border-r dark:bg-gray-800 dark:border-gray-700">
                <blockquote className="max-w-2xl mx-auto mb-4 text-gray-500 lg:mb-8 dark:text-gray-400">
                  <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                    Mindblowing workflow
                  </h3>
                  <p className="my-4">
                    Aesthetically, the well designed components are beautiful
                    and will undoubtedly level up your next application."
                  </p>
                </blockquote>
                <figcaption className="flex items-center justify-center space-x-3">
                  <img
                    className="rounded-full w-9 h-9"
                    src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/avatars/jese-leos.png"
                    alt="profile picture"
                  />
                  <div className="space-y-0.5 font-medium dark:text-white text-left">
                    <div>Jese Leos</div>
                    <div className="text-sm text-gray-500 dark:text-gray-400">
                      Software Engineer at Facebook
                    </div>
                  </div>
                </figcaption>
              </figure>
              <figure className="flex flex-col items-center justify-center p-8 text-center bg-white border-gray-200 rounded-b-lg md:rounded-br-lg dark:bg-gray-800 dark:border-gray-700">
                <blockquote className="max-w-2xl mx-auto mb-4 text-gray-500 lg:mb-8 dark:text-gray-400">
                  <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                    Efficient Collaborating
                  </h3>
                  <p className="my-4">
                    You have many examples that can be used to create a fast
                    prototype for your team."
                  </p>
                </blockquote>
                <figcaption className="flex items-center justify-center space-x-3">
                  <img
                    className="rounded-full w-9 h-9"
                    src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/avatars/joseph-mcfall.png"
                    alt="profile picture"
                  />
                  <div className="space-y-0.5 font-medium dark:text-white text-left">
                    <div>Joseph McFall</div>
                    <div className="text-sm text-gray-500 dark:text-gray-400">
                      CTO at Google
                    </div>
                  </div>
                </figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
