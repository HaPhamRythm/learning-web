import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Input, Select, message } from "antd";
import { setIsAddUser } from "../../../redux/modalSlice";
import { localServ } from "../../../services/localStoreService";
import { https } from "../../../services/config";
import { NavLink, useNavigate } from "react-router-dom";
import { AiFillCloseCircle } from "react-icons/ai";

export default function AddUser() {
  const { isAddUser } = useSelector((state) => state.modalSlice);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const accessToken = localServ.getAccessToken();
  const [form] = Form.useForm();

  let handleCloseModal = () => {
    dispatch(setIsAddUser(false));
  };

  const onFinish = (values) => {
    https
      .post("/api/QuanLyNguoiDung/ThemNguoiDung", values, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((res) => {
        message.success("Đăng ký thành công");

        setTimeout(() => {
          navigate("/login");
        }, 2000);
        console.log(res);
      })
      .catch((err) => {
        message.error("Tài khoản đã tồn tại hoặc thiếu thông tin");
        console.log(err);
      });
  };
  return isAddUser ? (
    // nhớ thêm class cho modal chớ sao mà nó tự hiển thị như modal được
    <div className="fixed inset-0 z-50 py-8 overflow-auto flex items-start justify-center bg-black bg-opacity-50">
      <div className="w-full sm:w-2/3 md:w-1/2 lg:w-1/3 text-slate-100 bg-slate-900 border border-slate-700 p-6 rounded-md drop-shadow-lg">
        <div className="flex items-center justify-between mb-4">
          <h2 className="text-xl font-bold text-center">THÊM NGƯỜI DÙNG</h2>

          {/* đóng modal */}
          <button onClick={handleCloseModal}>
            <AiFillCloseCircle className="w-6 h-6" />
          </button>
        </div>
        <Form
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          layout="vertical"
        >
          <Form.Item
            name="taiKhoan"
            label="Tài khoản"
            rules={[
              {
                required: true,
                message: "Tài khoản không được để trống",
              },
            ]}
          >
            <Input id="taiKhoan" />
          </Form.Item>
          <Form.Item
            label="Mật khẩu"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Mật khẩu không được để trống",
              },
            ]}
          >
            <Input.Password id="matKhau" />
          </Form.Item>
          <Form.Item
            name="hoTen"
            label="Họ Tên"
            rules={[
              {
                required: true,
                message: "Họ Tên không được để trống",
              },
            ]}
          >
            <Input id="hoTen" />
          </Form.Item>
          <Form.Item
            name="email"
            label="Email"
            rules={[
              {
                required: true,
                message: "Email không được để trống",
              },
            ]}
          >
            <Input id="email" />
          </Form.Item>
          <Form.Item
            name="soDt"
            label="Số điện thoại"
            rules={[
              {
                required: true,
                message: "Số điện thoại không được để trống",
              },
            ]}
          >
            <Input id="soDt" />
          </Form.Item>

          <Form.Item
            label="Mã nhóm"
            name="maNhom"
            rules={[
              {
                required: true,
                message: "Chọn 1 mã nhóm",
              },
            ]}
          >
            <Select>
              <Select.Option value="GP01">GP01</Select.Option>
              <Select.Option value="GP02">GP02</Select.Option>
              <Select.Option value="GP03">GP03</Select.Option>
              <Select.Option value="GP04">GP04</Select.Option>
              <Select.Option value="GP05">GP05</Select.Option>
              <Select.Option value="GP06">GP06</Select.Option>
              <Select.Option value="GP07">GP07</Select.Option>
              <Select.Option value="GP08">GP08</Select.Option>
              <Select.Option value="GP09">GP09</Select.Option>
              <Select.Option value="GP10">GP10</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Nhóm Người Dùng"
            name="maLoaiNguoiDung"
            rules={[
              {
                required: true,
                message: "Chọn 1 nhóm",
              },
            ]}
          >
            <Select>
              <Select.Option value="GV">Giáo vụ</Select.Option>
              <Select.Option value="HV">Học Viên</Select.Option>
            </Select>
          </Form.Item>

          <Form.Item>
            <div className="flex space-x-4">
              <button className="btn btn-primary text-base" type="submit">
                Thêm người dùng
              </button>
              {/* đóng modal */}
              <button
                onClick={handleCloseModal}
                className="btn btn-secondary text-base"
              >
                Đóng
              </button>
            </div>
          </Form.Item>
        </Form>
        <div className="text-right">
          <p>
            Bạn đã có tài khoản? &ndash;{" "}
            <NavLink
              to={"/login"}
              className="text-blue-500 hover:text-blue-600 font-medium"
            >
              Đăng nhập
            </NavLink>
          </p>
        </div>
      </div>
    </div>
  ) : null;
}
