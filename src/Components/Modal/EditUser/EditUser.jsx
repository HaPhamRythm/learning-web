import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { https } from "../../../services/config";
import {
  setIsEditUser,
  setUserAccessTokenEdit,
} from "../../../redux/modalSlice";
import { MdOutlineClose } from "react-icons/md";
import { message } from "antd";

export default function EditUser() {
  const { isEditUser, userAccessTokenEdit, passwordEdit, userIdEdit } =
    useSelector((state) => state.modalSlice);
  const dispatch = useDispatch();
  const initialInfo = {
    taiKhoan: "",
    matKhau: "",
    hoTen: "",
    soDT: "",
    maLoaiNguoiDung: "",
    maNhom: "",
    email: "",
  };

  const [editedUser, setEditedUser] = useState(initialInfo);
  const [userList, setUserList] = useState([]);

  useEffect(() => {
    if (userIdEdit) {
      https
        .post("/api/QuanLyNguoiDung/DangNhap", {
          taiKhoan: userIdEdit,
          matKhau: passwordEdit,
        })
        .then((res) => {
          setEditedUser({
            taiKhoan: res.data.taiKhoan,
            matKhau: passwordEdit,
            hoTen: res.data.hoTen,
            soDT: res.data.soDT,
            maLoaiNguoiDung: res.data.maLoaiNguoiDung,
            maNhom: res.data.maNhom,
            email: res.data.email,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [userIdEdit, passwordEdit]);

  const handleSubmit = (event) => {
    event.preventDefault();
    https
      .put("/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", editedUser, {
        headers: {
          Authorization: `Bearer ${userAccessTokenEdit}`,
        },
      })
      .then((res) => {
        message.success("Cập nhật thành công");

        https
          .get("/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP01")
          .then((res) => {
            setUserList(res.data);
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        message.err("Chưa cập nhật thành công");
        console.log(err);
      });
  };

  let resetUserInfo = () => {
    setEditedUser(initialInfo);
  };

  let handleCloseModal = () => {
    dispatch(setIsEditUser(false));
    dispatch(setUserAccessTokenEdit(""));
    resetUserInfo();
  };

  return isEditUser ? (
    <div
      className="modal modal-dialog-centered fixed inset-0 z-50 grid place-items-center p-4 overflow-x-hidden overflow-y-auto bg-gray-900 bg-opacity-75"
      id="myModal"
    >
      <div className="relative p-4 w-full max-w-2xl h-full md:h-auto">
        <div className="relative p-4 bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
          {/* Modal header */}
          <div className="flex justify-between items-center pb-4 mb-4 rounded-t border-b sm:mb-5 dark:border-gray-600">
            <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
              CẬP NHẬT NGƯỜI DÙNG
            </h3>
            <button
              onClick={handleCloseModal}
              type="button"
              className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
            >
              <MdOutlineClose className="w-5 h-5" />
              <span className="sr-only">Đóng</span>
            </button>
          </div>
          <form onSubmit={handleSubmit}>
            <div className="grid gap-4 mb-4 sm:grid-cols-2">
              <div className="mb-3 hidden">
                <label
                  htmlFor="taiKhoan"
                  className="block mb-2 text-sm font-medium text-slate-900 dark:text-slate-100"
                >
                  Tài khoản
                </label>
                <input
                  value={editedUser.taiKhoan}
                  type="text"
                  id="taiKhoan"
                  className="movie-input cursor-not-allowed"
                  required
                  disabled
                />
              </div>
              <div className="mb-3">
                <label
                  htmlFor="matKhau"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Mật khẩu
                </label>
                <input
                  // get matKhau in api to form
                  value={editedUser.matKhau}
                  onChange={(e) =>
                    setEditedUser((prevState) => ({
                      ...prevState,
                      matKhau: e.target.value,
                    }))
                  }
                  type="password"
                  id="matKhau"
                  className="movie-input"
                  required
                />
              </div>
              <div className="mb-3">
                <label
                  htmlFor="hoTen"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Họ tên
                </label>
                <input
                  value={editedUser.hoTen}
                  onChange={(e) =>
                    setEditedUser((prevState) => ({
                      ...prevState,
                      hoTen: e.target.value,
                    }))
                  }
                  type="text"
                  id="hoTen"
                  className="movie-input"
                  placeholder="Vd: PBT MOVIE"
                  required
                />
              </div>
              <div className="mb-3">
                <label
                  htmlFor="email"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Email
                </label>
                <input
                  value={editedUser.email}
                  onChange={(e) =>
                    setEditedUser((prevState) => ({
                      ...prevState,
                      email: e.target.value,
                    }))
                  }
                  type="email"
                  id="email"
                  className="movie-input"
                  placeholder="Vd: email@pbtmovie.com"
                  required
                />
              </div>
              <div className="mb-3">
                <label
                  htmlFor="soDT"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Số điện thoại
                </label>
                <input
                  value={editedUser.soDT}
                  onChange={(e) =>
                    setEditedUser((prevState) => ({
                      ...prevState,
                      soDT: e.target.value,
                    }))
                  }
                  type="text"
                  id="soDT"
                  className="movie-input"
                  placeholder="Ví dụ: 01234567"
                  required
                />
              </div>
              <div className="mb-3">
                <label
                  htmlFor="maNhom"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Mã nhóm
                </label>
                <input
                  value={editedUser.maNhom}
                  onChange={(e) =>
                    setEditedUser((prevState) => ({
                      ...prevState,
                      maNhom: e.target.value,
                    }))
                  }
                  type="text"
                  id="maNhom"
                  className="movie-input"
                  required
                />
              </div>

              <div className="mb-3">
                <label
                  htmlFor="maLoaiNguoiDung"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-slate-100"
                >
                  Mã loại người dùng
                </label>
                <input
                  value={editedUser.maLoaiNguoiDung}
                  onChange={(e) =>
                    setEditedUser((prevState) => ({
                      ...prevState,
                      maLoaiNguoiDung: e.target.value,
                    }))
                  }
                  type="text"
                  id="maLoaiNguoiDung"
                  className="movie-input"
                  required
                />
              </div>
            </div>
            <div className="flex items-center justify-center">
              <button type="submit" className="btn btn-primary w-1/10">
                Cập nhật
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  ) : null;
}
