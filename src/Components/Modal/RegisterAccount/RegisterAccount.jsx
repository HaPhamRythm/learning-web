import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { https } from "../../../services/config";
import { localServ } from "../../../services/localStoreService";
import { useState } from "react";
import { useEffect } from "react";
import { MdOutlineClose } from "react-icons/md";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
import { setIsRegisterCourse } from "../../../redux/modalSlice";
import { message } from "antd";

export default function RegisterAccount() {
  const { isRegisterCourse, courseId } = useSelector(
    (state) => state.modalSlice
  );
  const accessToken = localServ.getAccessToken();
  const [unregisteredUsers, setUnregisteredUsers] = useState([]);
  const [pendingUsers, setPendingUsers] = useState([]);
  const [registeredUsers, setRegisteredUsers] = useState([]);
  const [userID, setUserID] = useState("");

  const dispatch = useDispatch();
  useEffect(() => {
    if (courseId) {
      https
        .post(
          "/api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh",
          {
            maKhoaHoc: courseId,
          },
          {
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },
          }
        )
        .then((res) => {
          setUnregisteredUsers(res.data);
        })
        .catch((err) => console.log(err));

      https
        .post(
          "/api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet",
          {
            maKhoaHoc: courseId,
          },
          {
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },
          }
        )
        .then((res) => {
          setPendingUsers(res.data);
        })
        .catch((err) => console.log(err));

      https
        .post(
          "/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc",
          {
            maKhoaHoc: courseId,
          },
          {
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },
          }
        )
        .then((res) => {
          setRegisteredUsers(res.data);
        })
        .catch((err) => console.log(err));
    }
  }, [courseId, accessToken]);

  // Pagination
  const postsPerPage = 5;
  const [currentPendingPage, setCurrentPendingPage] = useState(1);
  const [currentRegisteredPage, setCurrentRegisteredPage] = useState(1);
  const startPendingIndex = (currentPendingPage - 1) * postsPerPage;
  const startRegisterIndex = (currentRegisteredPage - 1) * postsPerPage;
  const pendingUsersToDisplay = pendingUsers.slice(
    startPendingIndex,
    currentPendingPage * postsPerPage
  );
  const registeredUsersToDisplay = registeredUsers.slice(
    startRegisterIndex,
    currentRegisteredPage * postsPerPage
  );
  const pendingPages = Math.ceil(pendingUsers.length / postsPerPage);
  const registerPages = Math.ceil(registeredUsers.length / postsPerPage);

  const handlePendingPages = (page) => {
    setCurrentPendingPage(page);
  };
  const handleRegisterPages = (page) => {
    setCurrentRegisteredPage(page);
  };

  let handleCloseModal = () => {
    dispatch(setIsRegisterCourse(false));
  };

  let handleChooseUser = (userAccount) => {
    setUserID(userAccount);
  };

  let handleRegisterUser = (event) => {
    event.preventDefault();
    https
      .post(
        "/api/QuanLyKhoaHoc/GhiDanhKhoaHoc",
        {
          maKhoaHoc: courseId,
          taiKhoan: userID,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      )
      .then((res) => {
        message.success(res.data);
        https
          .post(
            "/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc",
            {
              maKhoaHoc: courseId,
            },
            {
              headers: {
                Authorization: `Bearer ${accessToken}`,
              },
            }
          )
          .then((res) => {
            setRegisteredUsers(res.data);
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => console.log(err));
  };

  let handleDeleteUser = (userAccount) => {
    https
      .post(
        "/api/QuanLyKhoaHoc/HuyGhiDanh",
        {
          maKhoaHoc: courseId,
          taiKhoan: userAccount,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      )
      .then((res) => {
        message.success(res.data);
        https
          .post(
            "/api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet",
            {
              maKhoaHoc: courseId,
            },
            {
              headers: {
                Authorization: `Bearer ${accessToken}`,
              },
            }
          )
          .then((res) => {
            setPendingUsers(res.data);
          })
          .catch((err) => console.log(err));

        https
          .post(
            "/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc",
            {
              maKhoaHoc: courseId,
            },
            {
              headers: {
                Authorization: `Bearer ${accessToken}`,
              },
            }
          )
          .then((res) => {
            setRegisteredUsers(res.data);
          })
          .catch((err) => console.log(err));
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };
  return isRegisterCourse ? (
    <div>
      {/* form when clicking "Ghi danh" */}
      <div className="fixed inset-0 z-50 flex items-start justify-center p-4 overflow-x-hidden overflow-y-auto bg-gray-900 bg-opacity-75">
        {/** adjust the width of popup screen, compared to the full screen */}
        <div className="relative  py-4 w-full max-w-2xl h-full md:h-auto">
          {/* set background color for popup screen */}
          <div className="relative  py-2 bg-white rounded-lg shadow dark:bg-gray-800 sm:p-5">
            {/* close button for popup screen */}
            <div className="flex justify-between items-center mb-1">
              <button
                onClick={handleCloseModal}
                type="button"
                className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
              >
                <MdOutlineClose className="w-5 h-5" />
                <span className="sr-only">Đóng</span>
              </button>
            </div>
            {/* search unregistered users -> register */}
            <div className="flex items-center justify-around">
              <div className="relative w-2/3 mb-4 mr-10 ">
                <form>
                  <label
                    htmlFor="data"
                    className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
                  >
                    Search
                  </label>
                  <div className="relative">
                    <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                      <svg
                        className="w-4 h-4 text-gray-500 dark:text-gray-400"
                        aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 20 20"
                      >
                        <path
                          stroke="currentColor"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={2}
                          d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                        />
                      </svg>
                    </div>
                    <input
                      type="search"
                      id="searchUnregisteredUsers"
                      className="block w-full p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                      placeholder="Tìm Học Viên chưa đăng ký"
                      required
                      onChange={(e) => {
                        handleChooseUser(e.target.value);
                      }}
                      list="unregisteredUsers"
                    />
                    <datalist id="unregisteredUsers">
                      {unregisteredUsers.map((user) => {
                        return (
                          <option
                            className="dropdown-row"
                            key={user.taiKhoan}
                            value={user.taiKhoan}
                          >
                            {user.hoTen}
                          </option>
                        );
                      })}
                    </datalist>
                  </div>
                </form>
              </div>
              <div className="relative w-1/3 mb-4">
                <button
                  type="submit"
                  className="text-white  right-2.5 bottom-2.5 bg-green-700 hover:bg-green-900 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-green-400 dark:hover:bg-green-700 dark:focus:ring-green-800 "
                  onClick={handleRegisterUser}
                >
                  Ghi danh
                </button>
              </div>
            </div>
            <h2 className=" mb-3 text-gray-900 uppercase dark:text-gray-400">
              Học viên chờ xác thực
            </h2>
            <table className="w-full text-xs md:text-sm text-left text-gray-500 dark:text-gray-400">
              <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th scope="col" className="px-6 py-3">
                    STT
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Tài khoản
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Họ tên
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Chờ xác nhận
                  </th>
                </tr>
              </thead>
              <tbody>
                {pendingUsersToDisplay.length > 0 ? (
                  pendingUsersToDisplay.map((user, index) => {
                    const absoluteIndex = startPendingIndex + 1 + index;
                    return (
                      <tr
                        key={user.taiKhoan}
                        className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
                      >
                        <th
                          scope="row"
                          className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                        >
                          {absoluteIndex}
                        </th>
                        <td className="px-4 py-2">{user.taiKhoan}</td>
                        <td className="px-4 py-2">{user.hoTen}</td>
                        <td className="px-12 py-2">
                          <button
                            className="font-medium text-red-600 dark:text-red-500 hover:underline"
                            onClick={() => handleDeleteUser(user.taiKhoan)}
                          >
                            Xoá
                          </button>
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <td className="w-4 p-4 text-center" colSpan={4}>
                      Không có khóa học chờ xét duyệt
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
            {/* pagination */}
            {pendingPages > 1 ? (
              <nav
                className="flex items-center justify-center pt-4"
                aria-label="Table navigation"
              >
                <ul className="inline-flex -space-x-px text-sm h-8">
                  <li>
                    <button
                      onClick={() => handlePendingPages(currentPendingPage - 1)}
                      disabled={currentPendingPage === 1}
                      className={`flex items-center justify-center px-3 h-8 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-l-lg dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
                        currentPendingPage === 1
                          ? "cursor-not-allowed"
                          : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                      }`}
                    >
                      <BsChevronLeft />
                    </button>
                  </li>
                  {Array.from({ length: pendingPages }, (_, index) => (
                    <li key={index}>
                      <button
                        onClick={() => handlePendingPages(index + 1)}
                        className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 dark:border-gray-700 dark:text-gray-400 ${
                          currentPendingPage === index + 1
                            ? "dark:bg-gray-700 dark:text-white"
                            : "dark:bg-gray-800 hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                        }`}
                      >
                        {index + 1}
                      </button>
                    </li>
                  ))}
                  <li>
                    <button
                      onClick={() => handlePendingPages(currentPendingPage + 1)}
                      disabled={currentPendingPage === pendingPages}
                      className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
                        currentPendingPage === pendingPages
                          ? "cursor-not-allowed"
                          : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                      }`}
                    >
                      <BsChevronRight />
                    </button>
                  </li>
                </ul>
              </nav>
            ) : null}

            <h2 className=" my-3 text-gray-900 uppercase dark:text-gray-400">
              Học viên đã ghi danh
            </h2>
            <table className="w-full text-xs md:text-sm text-left text-gray-500 dark:text-gray-400">
              <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                  <th scope="col" className="px-6 py-3">
                    STT
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Tài khoản
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Họ tên
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Chờ xác nhận
                  </th>
                </tr>
              </thead>
              <tbody>
                {registeredUsersToDisplay.length > 0 ? (
                  registeredUsersToDisplay.map((user, index) => {
                    const absoluteIndex = startRegisterIndex + 1 + index;
                    return (
                      <tr
                        key={user.taiKhoan}
                        className="bg-white border-b dark:bg-gray-900 dark:border-gray-700"
                      >
                        <th
                          scope="row"
                          className="px-4 py-2 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                        >
                          {absoluteIndex}
                        </th>
                        <td className="px-4 py-2">{user.taiKhoan}</td>
                        <td className="px-4 py-2">{user.hoTen}</td>

                        <td className="px-12 py-2">
                          <button
                            className="font-medium text-red-600 dark:text-red-500 hover:underline"
                            onClick={() => handleDeleteUser(user.taiKhoan)}
                          >
                            Xoá
                          </button>
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <td className="w-4 p-4 text-center" colSpan={4}>
                      Chưa có khóa học nào được đăng ký
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
            {/* pagination */}
            {registerPages > 1 ? (
              <nav
                className="flex items-center justify-center pt-4"
                aria-label="Table navigation"
              >
                <ul className="inline-flex -space-x-px text-sm h-8">
                  <li>
                    <button
                      onClick={() =>
                        handleRegisterPages(currentRegisteredPage - 1)
                      }
                      disabled={currentRegisteredPage === 1}
                      className={`flex items-center justify-center px-3 h-8 ml-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-l-lg dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
                        currentRegisteredPage === 1
                          ? "cursor-not-allowed"
                          : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                      }`}
                    >
                      <BsChevronLeft />
                    </button>
                  </li>
                  {Array.from({ length: registerPages }, (_, index) => (
                    <li key={index}>
                      <button
                        onClick={() => handleRegisterPages(index + 1)}
                        className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 dark:border-gray-700 dark:text-gray-400 ${
                          currentRegisteredPage === index + 1
                            ? "dark:bg-gray-700 dark:text-white"
                            : "dark:bg-gray-800 hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                        }`}
                      >
                        {index + 1}
                      </button>
                    </li>
                  ))}
                  <li>
                    <button
                      onClick={() =>
                        handleRegisterPages(currentRegisteredPage + 1)
                      }
                      disabled={currentRegisteredPage === registerPages}
                      className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-r-lg dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 ${
                        currentRegisteredPage === registerPages
                          ? "cursor-not-allowed"
                          : "hover:bg-gray-100 hover:text-gray-700 dark:hover:bg-gray-700 dark:hover:text-white"
                      }`}
                    >
                      <BsChevronRight />
                    </button>
                  </li>
                </ul>
              </nav>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  ) : null;
}
