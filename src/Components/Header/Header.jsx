import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";
import PageNav from "./PageNav";
import "./Header.scss";

export default function Header() {
  return (
    <header className="bg-white py-2 shadow">
      <div className="container flex items-center justify-between">
        <NavLink to={"/"} className="brand text-xl font-bold">
          E-Learning
        </NavLink>

        <PageNav />

        <UserNav />
      </div>
    </header>
  );
}
