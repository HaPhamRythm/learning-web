import React from "react";

import { NavLink } from "react-router-dom";
import { BsChevronDown, BsFlower1 } from "react-icons/bs";
import { localServ } from "../../services/localStoreService";
import { useSelector } from "react-redux";
import UserNavDesktop from "./UserNavDesktop";
import { Desktop } from "../../responsive/responsive";

export default function UserNav() {
  return (
    <nav className="user">
      <Desktop>
        <UserNavDesktop />
      </Desktop>
    </nav>
  );
}
