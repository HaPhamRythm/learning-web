import React from "react";
import { useSelector } from "react-redux";
import { localServ } from "../../services/localStoreService";
import { NavLink } from "react-router-dom";
import { BsChevronDown } from "react-icons/bs";

export default function UserNavDesktop() {
  let userLogin = useSelector((state) => state.userSlice.userInfo);
  let handleLogout = () => {
    localServ.removeUser();
    localServ.removeAccessToken();
    window.location.href = "/";
  };
  return userLogin ? (
    <div className="user-dropdown ">
      {/* dropdown button */}
      <button className="user-dropdown__btn">
        {/* avatar */}
        <div className="user-dropdown__avatar">
          <img
            src={`https://ui-avatars.com/api/?background=random&name=${userLogin.hoTen}`}
            alt={userLogin.hoTen}
            className="w-full h-full object-cover"
          />
        </div>
        <BsChevronDown />
      </button>

      {/* dropdown for users */}

      <ul className="user-dropdown__nav">
        {userLogin.maLoaiNguoiDung === "GV" && (
          <>
            <li>
              <NavLink to="/admin/khoa-hoc">Quản Lý Khóa Học</NavLink>
            </li>
            <li>
              <NavLink to="/admin/nguoi-dung">Quản Lý Người Dùng</NavLink>
            </li>
          </>
        )}

        <li>
          <NavLink to="/account">Thông Tin Tài khoản</NavLink>
        </li>
        <li>
          <NavLink onClick={handleLogout}>Đăng xuất</NavLink>
        </li>
      </ul>
    </div>
  ) : (
    <ul className="user-nav">
      <li>
        <NavLink to={"/register"} className="btn btn-pill btn-secondary">
          Đăng ký
        </NavLink>
      </li>
      <li>
        <NavLink to={"/login"} className="btn btn-pill btn-primary">
          Đăng nhập
        </NavLink>
      </li>
    </ul>
  );
}
