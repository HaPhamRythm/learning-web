import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./Layout/Layout";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import CoursePage from "./Pages/CoursePage/Course";
import BlogPage from "./Pages/BlogPage/BlogPage";
import EventPage from "./Pages/EventPage/EventPage";
import InfoPage from "./Pages/InfoPage/InfoPage";
import AccountPage from "./Pages/AccountPage/AccountPage";
import AdminPage from "./Pages/AdminPage/AdminPage";

import AdminCoursePage from "./Pages/AdminPage/AdminCoursePage";
import AdminLayout from "./Layout/AdminLayout";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout contentPage={<HomePage />} />} />
          <Route
            path="/course"
            element={<Layout contentPage={<CoursePage />} />}
          />
          <Route path="/blog" element={<Layout contentPage={<BlogPage />} />} />
          <Route
            path="/event"
            element={<Layout contentPage={<EventPage />} />}
          />
          <Route path="/info" element={<Layout contentPage={<InfoPage />} />} />
          <Route
            path="/login"
            element={<Layout contentPage={<LoginPage />} />}
          />
          <Route
            path="/register"
            element={<Layout contentPage={<RegisterPage />} />}
          />
          <Route
            path="/account"
            element={<Layout contentPage={<AccountPage />} />}
          />
          <Route
            path="/admin/nguoi-dung"
            element={<AdminLayout contentPage={<AdminPage />} />}
          />
          <Route
            path="/admin/khoa-hoc"
            element={<AdminLayout contentPage={<AdminCoursePage />} />}
          />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
